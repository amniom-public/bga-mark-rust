export const trackingFunction = (cta_id: string) => {    
    if(gtag) {
        gtag('event', 'cta_pressed', { cta_id: cta_id });
    }
}